# Weather 4


* Kotlin
* multi-module app
* MVVM, coroutine
* lifecycle
* koin
* room
* gson
* retrofit
* navigation
* databinding

[Verions libs](https://gitlab.com/Likandr/weather-4/blob/master/buildSrc/src/main/kotlin/Versions.kt)