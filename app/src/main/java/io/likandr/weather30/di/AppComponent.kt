package io.likandr.weather30.di

import io.likandr.detail.di.featureDetailModule
import io.likandr.home.di.featureHomeModule
import io.likandr.local.di.localModule
import io.likandr.remote.NetConst.API_BASE_URL
import io.likandr.remote.di.createRemoteModule
import io.likandr.repository.di.repositoryModule
import io.likandr.search.di.featureSearchModule

val appComponent = listOf(
    appModule,
    createRemoteModule(API_BASE_URL),
    repositoryModule,
    featureHomeModule,
    featureSearchModule,
    featureDetailModule,
    localModule
)