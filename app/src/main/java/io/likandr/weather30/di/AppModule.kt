package io.likandr.weather30.di

import io.likandr.common.WeatherPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val appModule = module {
    factory { WeatherPreferences(androidContext()) }
}