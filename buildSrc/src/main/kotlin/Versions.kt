object Releases {
    val versionCode = 1
    val versionName = "1.0"
}

object Versions {
    val kotlin = "1.3.41"
    val gradle = "3.5.0"
    val navigation = "2.1.0-alpha02"
    // Sdk and tools
    val compileSdk = 28
    val minSdk = 21
    val targetSdk = 28
    // App dependencies
    val appCompat = "1.1.0-alpha04"
    val coreKtx = "1.1.0-alpha04"
    val constraintLayout = "1.1.3"
    val junit = "4.12"
    val androidTestRunner = "1.1.2-alpha02"
    val espressoCore = "3.2.0-alpha02"
    val retrofit = "2.5.0"
    val retrofitCoroutines = "0.9.2"
    val retrofitGson = "2.4.0"
    val gson = "2.8.5"
    val okHttp = "3.12.1"
    val coroutines = "1.1.1"
    val koin = "1.0.2"
    val timber = "4.7.1"
    val lifecycle = "2.1.0-alpha04"
    val nav = "2.0.0"
    val room = "2.1.0-alpha06"
    val recyclerview = "1.0.0"
    val glide = "4.9.0"
    val mockwebserver = "2.7.5"
    val archCoreTest = "2.0.0"
    val androidJunit = "1.1.0"
    val mockk = "1.9.2"
    val fragmentTest = "1.1.0-alpha06"
    val databinding = "3.3.2"
}

object ApplicationId {
    val id = "io.likandr.weather30"
}