package io.likandr.common

import android.content.Context
import io.likandr.common.utils.Preferences

class WeatherPreferences(context: Context) : Preferences(context) {
    var favoriteCities by stringSetPref("CITIES_FAVORITE", hashSetOf("524901", "519690"))
}