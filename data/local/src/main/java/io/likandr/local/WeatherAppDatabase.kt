package io.likandr.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.likandr.local.converter.Converters
import io.likandr.local.converter.DailyForecastTypeConverter
import io.likandr.local.converter.WeatherTypeConverter
import io.likandr.local.dao.CityFavDao
import io.likandr.local.dao.ForecastDao
import io.likandr.model.entity.CityEntity
import io.likandr.model.entity.CityFavEntity
import io.likandr.model.entity.DailyForecastEntity

@Database(
    entities = [CityFavEntity::class, CityEntity::class, DailyForecastEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    Converters::class,
    DailyForecastTypeConverter::class,
    WeatherTypeConverter::class
)
abstract class WeatherAppDatabase : RoomDatabase() {

//    abstract fun cityDao(): CityDao
    abstract fun cityFavDao(): CityFavDao
    abstract fun forecastDao(): ForecastDao

    companion object {
        fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            WeatherAppDatabase::class.java,
            "Weather3App.db"
        ).build()
    }
}