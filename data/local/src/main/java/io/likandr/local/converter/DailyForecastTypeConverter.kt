package io.likandr.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.likandr.model.DailyForecast

class DailyForecastTypeConverter {
    private val gson = Gson()
    private val type = object : TypeToken<MutableList<DailyForecast>>() {}.type

    @TypeConverter
    fun fromString(json: String): MutableList<DailyForecast> {
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun toString(dailyForecast: MutableList<DailyForecast>): String {
        return gson.toJson(dailyForecast, type)
    }
}
