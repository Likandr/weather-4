package io.likandr.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.likandr.model.Weather

class WeatherTypeConverter {
    private val gson = Gson()
    private val type = object : TypeToken<MutableList<Weather>>() {}.type

    @TypeConverter
    fun fromString(json: String): MutableList<Weather> {
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun toString(dailyForecast: MutableList<Weather>): String {
        return gson.toJson(dailyForecast, type)
    }
}