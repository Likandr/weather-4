package io.likandr.local.dao

import androidx.room.Dao
import androidx.room.Query
import io.likandr.model.entity.CityFavEntity
import java.util.*

@Dao
abstract class CityFavDao : BaseDao<CityFavEntity>() {

    @Query("SELECT * FROM city_fav WHERE id IN (:ids)")
    abstract suspend fun getByIds(ids: List<String>): List<CityFavEntity>

    @Query("SELECT * FROM city_fav")
    abstract suspend fun getAll(): List<CityFavEntity>

    @Query("DELETE FROM city_fav")
    abstract suspend fun deleteAll()

    @Query("SELECT id FROM city_fav")
    abstract suspend fun getCityIds(): List<String>

    @Query("SELECT COUNT(*) from city_fav")
    abstract suspend fun count(): Int
    //---
    suspend fun save(cityEntities: List<CityFavEntity>) {
        insert(cityEntities.apply { forEach { it.lastRefreshed = Date() } })
    }

    suspend fun remove(cityEntities: List<CityFavEntity>) {
        delete(cityEntities.apply { forEach { it.lastRefreshed = Date() } })
    }
}