package io.likandr.local.dao

import androidx.room.Dao
import androidx.room.Query
import io.likandr.model.entity.DailyForecastEntity
import java.util.*

@Dao
abstract class ForecastDao : BaseDao<DailyForecastEntity>() {

    @Query("SELECT * FROM forecast_daily WHERE id=:id")
    abstract suspend fun getById(id : Int): DailyForecastEntity
    //---
    suspend fun save(forecast: DailyForecastEntity) {
        insert(forecast.apply { lastRefreshed = Date() })
    }
}