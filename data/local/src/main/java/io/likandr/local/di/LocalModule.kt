package io.likandr.local.di

import io.likandr.local.WeatherAppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

private const val DATABASE = "DATABASE"

val localModule = module {
    single(DATABASE) { WeatherAppDatabase.buildDatabase(androidContext()) }
//    factory { (get(DATABASE) as WeatherAppDatabase).cityDao() }
    factory { (get(DATABASE) as WeatherAppDatabase).cityFavDao() }
    factory { (get(DATABASE) as WeatherAppDatabase).forecastDao() }
}