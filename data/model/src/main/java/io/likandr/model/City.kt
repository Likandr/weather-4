package io.likandr.model

data class City(
    val id: String,
    val name: String,
    val coord: Coord,
    val country: String?,
    val population: Int?
)
