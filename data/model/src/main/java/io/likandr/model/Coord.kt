package io.likandr.model

data class Coord(
    val lon: Double,
    val lat: Double
)