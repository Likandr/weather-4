package io.likandr.model

import androidx.room.Embedded

data class DailyForecast(
    val dt: Long?,
    @Embedded val temp: Temp?,
    val pressure: Double?,
    val humidity: Int?,
    @Embedded
    val weather: MutableList<Weather>? = null,
    val speed: Double?,
    val deg: Int?,
    val clouds: Int?,
    val rain: Double?
)