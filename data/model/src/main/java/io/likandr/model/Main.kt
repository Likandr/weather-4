package io.likandr.model

data class Main(
    val temp: Double,
    val pressure: String,
    val humidity: String,
    val temp_min: String,
    val temp_max: String,
    val sea_level: String?,
    val grnd_level: String?
)