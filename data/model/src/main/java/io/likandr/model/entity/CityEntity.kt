package io.likandr.model.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import io.likandr.model.Coord
import io.likandr.model.Main
import java.util.*
import java.util.concurrent.TimeUnit

@Entity(tableName = "city_search")
data class CityEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    @Embedded val coord: Coord,
    @Embedded val main: Main,
    val country: String?,
    val population: Int?,
    var lastRefreshed: Date
) {
    /**
     * We consider that an [CityEntity] is outdated when the last time
     * we fetched it was more than 10 minutes
     */
    fun haveToRefreshFromNetwork(): Boolean =
        TimeUnit.MILLISECONDS.toMinutes(Date().time - lastRefreshed.time) >= 10
}
