package io.likandr.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.likandr.model.DailyForecast
import java.util.*
import java.util.concurrent.TimeUnit

@Entity(tableName = "forecast_daily")
data class DailyForecastEntity(
    @PrimaryKey
    val id: String = "",
    val name: String = "",
    val cnt: Int = -1,
    val list: MutableList<DailyForecast> = mutableListOf(),
    var lastRefreshed: Date? = Date(0)
) {
    /**
     * We consider that an [CityFavEntity] is outdated when the last time
     * we fetched it was more than 10 minutes
     */
    fun haveToRefreshFromNetwork(): Boolean =
        TimeUnit.MILLISECONDS.toMinutes(Date().time - (lastRefreshed?.time ?: 0)) >= 10
}