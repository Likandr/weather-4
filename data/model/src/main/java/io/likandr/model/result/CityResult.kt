package io.likandr.model.result

import com.google.gson.annotations.SerializedName

data class CityResult<T>(
    @SerializedName("cnt") val totalCount: Int,
    @SerializedName("list") val list: List<T>
)
