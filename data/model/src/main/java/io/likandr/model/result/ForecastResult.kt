package io.likandr.model.result

import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import io.likandr.model.City
import io.likandr.model.DailyForecast

@Entity(tableName = "forecast_result")
data class ForecastResult(
    @Embedded @SerializedName("city") val city: City,
    @SerializedName("cod") val cod: String,
    @SerializedName("message") val message: Double,
    @SerializedName("cnt") val totalCount: Int,
    @Embedded
    @SerializedName("list")
    val list: MutableList<DailyForecast> = mutableListOf()
)
