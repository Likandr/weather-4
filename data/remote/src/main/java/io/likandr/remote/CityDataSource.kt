package io.likandr.remote

/**
 * Implementation of [CityService] interface
 */
class CityDataSource(private val userService: CityService) {

    /**
     * Get the current weather forecast for a list of cities by [ids] on [lng] language.
     */
    fun fetchCurrentWeatherAsync(ids: List<String>, lng: String) =
        userService.fetchCurrentWeatherAsync(
            ids.joinToString(separator = ",").trim().ifEmpty { "524901,519690" },
            lng.trim().ifEmpty { "en" })

    /**
     * Get a list of cities as a result of a city search by [name]
     */
    fun fetchSearchCityAsync(name: String) =
        userService.fetchSearchCityAsync(name)

    /**
     * Get weather forecast by city [id] for [cnt] days on [lng] language.
     */
    fun fetchForecastAsync(id: Int, lng: String, cnt: Int) =
        userService.fetchForecastAsync(id, lng, cnt)
}