package io.likandr.remote

import io.likandr.model.entity.CityEntity
import io.likandr.model.entity.CityFavEntity
import io.likandr.model.result.CityResult
import io.likandr.model.result.ForecastResult
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface CityService {

    @GET(NetConst.CITY_DATA)
    fun fetchCurrentWeatherAsync(
        @Query("id") idsOfCity: String,
        @Query("lang") lang: String
    ): Deferred<CityResult<CityFavEntity>>

    @GET(NetConst.CITY_FIND)
    fun fetchSearchCityAsync(@Query("q") search: String): Deferred<CityResult<CityEntity>>

    @GET(NetConst.FORECAST_DAILY)
    fun fetchForecastAsync(
        @Query("id") idsOfCity: Int,
        @Query("lang") lang: String,
        @Query("cnt") countDays: Int
    ): Deferred<ForecastResult>
}