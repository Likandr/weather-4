package io.likandr.remote

object NetConst {
    const val API_BASE_URL = "https://api.openweathermap.org/"
    private const val APPID = "cc3835bdac9edc08be06c2499f35026e"

    const val CITY_DATA = "data/2.5/group?units=metric&appid=$APPID"
    const val CITY_FIND = "data/2.5/find?units=metric&appid=$APPID"
    const val FORECAST_DAILY = "data/2.5/forecast/daily?&mode=json&units=metric&appid=$APPID"
}