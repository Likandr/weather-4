package io.likandr.repository

import androidx.lifecycle.LiveData
import io.likandr.model.entity.CityEntity
import io.likandr.model.entity.CityFavEntity
import io.likandr.repository.utils.Resource

interface CityRepository {
    suspend fun getCityWithCache(forceRefresh: Boolean = false, ids_of_city: List<String>, lng: String): LiveData<Resource<List<CityFavEntity>>>
    suspend fun searchCityNoCache(nameOfCity: String): LiveData<Resource<List<CityEntity>>>
}