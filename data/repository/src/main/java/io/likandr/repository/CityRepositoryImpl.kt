package io.likandr.repository

import androidx.lifecycle.LiveData
import io.likandr.local.dao.CityFavDao
import io.likandr.model.result.CityResult
import io.likandr.model.entity.CityEntity
import io.likandr.model.entity.CityFavEntity
import io.likandr.remote.CityDataSource
import io.likandr.repository.utils.NetworkBoundResource
import io.likandr.repository.utils.NetworkBoundResourceNoCache
import io.likandr.repository.utils.Resource
import kotlinx.coroutines.Deferred

/**
 * Implementation of [CityRepository] interface
 */
class CityRepositoryImpl(
    private val dataSource: CityDataSource,
    private val cityFavDao: CityFavDao
) : CityRepository {

    /**
     * Suspended function that will get a list of [CityFavEntity]
     * whether in cache (Room) or via network (API).
     * [NetworkBoundResource] is responsible to handle this behavior.
     */
    override suspend fun getCityWithCache(
        forceRefresh: Boolean,
        ids_of_city: List<String>,
        lng: String
    ): LiveData<Resource<List<CityFavEntity>>> {
        return object : NetworkBoundResource<List<CityFavEntity>, CityResult<CityFavEntity>>() {
            override fun processResponse(response: CityResult<CityFavEntity>): List<CityFavEntity> =
                response.list

            override suspend fun saveCallResults(items: List<CityFavEntity>) =
                cityFavDao.save(items)

            override fun shouldFetch(data: List<CityFavEntity>?): Boolean =
                data == null || data.isEmpty() || forceRefresh

            override suspend fun loadFromDb(): List<CityFavEntity> =
                cityFavDao.getByIds(ids_of_city)

            override fun createCallAsync(): Deferred<CityResult<CityFavEntity>> =
                dataSource.fetchCurrentWeatherAsync(ids_of_city, lng)

        }.build().asLiveData()
    }

    /**
     * Suspended function that will get a list of [CityEntity]
     * from network (API).
     * [NetworkBoundResourceNoCache] is responsible to handle this behavior.
     */
    override suspend fun searchCityNoCache(nameOfCity: String): LiveData<Resource<List<CityEntity>>> {
        return object : NetworkBoundResourceNoCache<List<CityEntity>, CityResult<CityEntity>>() {
            override fun processResponse(response: CityResult<CityEntity>): List<CityEntity> =
                response.list

            override fun createCallAsync(): Deferred<CityResult<CityEntity>> =
                dataSource.fetchSearchCityAsync(nameOfCity)
        }.build().asLiveData()
    }
}