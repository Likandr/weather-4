package io.likandr.repository

import androidx.lifecycle.LiveData
import io.likandr.model.entity.DailyForecastEntity
import io.likandr.repository.utils.Resource

interface ForecastRepository {
    suspend fun getForecastWithCache(
        forceRefresh: Boolean,
        id: Int,
        lng: String,
        cnt: Int
    ): LiveData<Resource<DailyForecastEntity>>
}