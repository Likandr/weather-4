package io.likandr.repository

import androidx.lifecycle.LiveData
import io.likandr.local.dao.ForecastDao
import io.likandr.model.DailyForecast
import io.likandr.model.entity.DailyForecastEntity
import io.likandr.model.result.ForecastResult
import io.likandr.remote.CityDataSource
import io.likandr.repository.utils.NetworkBoundResource
import io.likandr.repository.utils.Resource
import kotlinx.coroutines.Deferred
import java.util.*

/**
 * Implementation of [ForecastRepository] interface
 */
class ForecastRepositoryImpl(
    private val dataSource: CityDataSource,
    private val dao: ForecastDao
) : ForecastRepository {

    /**
     * Suspended function that will get [DailyForecastEntity] include a list of [DailyForecast]
     * whether in cache (Room) or via network (API).
     * [NetworkBoundResource] is responsible to handle this behavior.
     */
    override suspend fun getForecastWithCache(
        forceRefresh: Boolean,
        id: Int,
        lng: String,
        cnt: Int
    ): LiveData<Resource<DailyForecastEntity>> {
        return object : NetworkBoundResource<DailyForecastEntity, ForecastResult>() {
            override fun processResponse(response: ForecastResult): DailyForecastEntity =
                transform(response)

            override suspend fun saveCallResults(items: DailyForecastEntity) =
                dao.save(items)

            override fun shouldFetch(data: DailyForecastEntity?): Boolean =
                data == null || data.list.isEmpty() || forceRefresh

            override suspend fun loadFromDb(): DailyForecastEntity =
                dao.getById(id)

            override fun createCallAsync(): Deferred<ForecastResult> =
                dataSource.fetchForecastAsync(id, lng, cnt)
        }.build().asLiveData()
    }

    /**
     * Function for converting [ForecastResult] objects to [DailyForecastEntity]
     */
    private fun transform(result: ForecastResult): DailyForecastEntity {
        return DailyForecastEntity(
            id = result.city.id,
            name = result.city.name,
            cnt = result.totalCount,
            list = result.list,
            lastRefreshed = Date()
        )
    }
}