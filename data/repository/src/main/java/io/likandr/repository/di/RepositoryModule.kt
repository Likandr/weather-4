package io.likandr.repository.di

import io.likandr.repository.AppDispatchers
import io.likandr.repository.CityRepositoryImpl
import io.likandr.repository.ForecastRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory { CityRepositoryImpl(get(), get()) }
    factory { ForecastRepositoryImpl(get(), get()) }
}