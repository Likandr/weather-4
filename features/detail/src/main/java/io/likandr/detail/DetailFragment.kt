package io.likandr.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.likandr.common.base.BaseFragment
import io.likandr.common.base.BaseViewModel
import io.likandr.detail.databinding.FragmentDetailBinding
import io.likandr.detail.views.DetailAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class DetailFragment : BaseFragment() {

    private val viewModel: DetailViewModel by viewModel()
    private lateinit var binding: FragmentDetailBinding

    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        setUpToolbar()
        return binding.root
    }

    private fun setUpToolbar() {
        setHasOptionsMenu(true)
        val activity = activity as AppCompatActivity
        val toolbar: Toolbar = binding.fragmentDetailToolbar
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.fragmentDetailTitle.text = args.cityName
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                findNavController().navigate(R.id.homeFragment); true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureRecyclerView()

        viewModel.loadData(args.cityId)
    }

    override fun getViewModel(): BaseViewModel = viewModel

    private fun configureRecyclerView() {
        binding.fragmentDetailRv.adapter = DetailAdapter(viewModel)
    }
}