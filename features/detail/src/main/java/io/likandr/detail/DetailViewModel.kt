package io.likandr.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.likandr.common.base.BaseViewModel
import io.likandr.common.utils.Event
import io.likandr.detail.domain.GetForecastUseCase
import io.likandr.model.entity.DailyForecastEntity
import io.likandr.repository.AppDispatchers
import io.likandr.repository.utils.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailViewModel(
    private val getForecastUseCase: GetForecastUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    private lateinit var argsId: String

    private val _forecast = MediatorLiveData<Resource<DailyForecastEntity>>()
    val dailyForecast: LiveData<Resource<DailyForecastEntity>> get() = _forecast
    private var forecastSource: LiveData<Resource<DailyForecastEntity>> = MutableLiveData()

    //region PUBLIC ACTIONS
    fun loadData(idCity: String) {
        argsId = idCity
        getCities(argsId, false)
    }

    fun refreshesItems() = getCities(argsId, true)
    //endregion

    private fun getCities(idCity: String, forceRefresh: Boolean) =
        viewModelScope.launch(dispatchers.main) {
            _forecast.removeSource(forecastSource)
            withContext(dispatchers.io) {
                forecastSource = getForecastUseCase(
                    forceRefresh,
                    idCity.toInt(),
                    "",
                    7
                )
            }
            _forecast.addSource(forecastSource) {
                _forecast.value = it
                if (it.status == Resource.Status.ERROR) _snackbarError.value =
                    Event(R.string.an_error_happened)
            }
        }
}