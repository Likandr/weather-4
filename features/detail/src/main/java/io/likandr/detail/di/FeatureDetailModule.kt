package io.likandr.detail.di

import io.likandr.detail.DetailViewModel
import io.likandr.detail.domain.GetForecastUseCase
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureDetailModule = module {
    factory { GetForecastUseCase(get()) }
    viewModel { DetailViewModel(get(), get()) }
}