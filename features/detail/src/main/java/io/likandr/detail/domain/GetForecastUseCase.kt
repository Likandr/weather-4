package io.likandr.detail.domain

import androidx.lifecycle.LiveData
import io.likandr.model.entity.DailyForecastEntity
import io.likandr.repository.ForecastRepositoryImpl
import io.likandr.repository.utils.Resource

class GetForecastUseCase(private val repository: ForecastRepositoryImpl) {
    suspend operator fun invoke(
        forceRefresh: Boolean = false,
        id: Int,
        lng: String,
        cnt: Int
    ): LiveData<Resource<DailyForecastEntity>> {
        return repository.getForecastWithCache(forceRefresh, id, lng, cnt)
    }
}