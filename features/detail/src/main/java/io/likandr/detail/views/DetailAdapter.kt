package io.likandr.detail.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.likandr.detail.DetailViewModel
import io.likandr.detail.R
import io.likandr.model.DailyForecast

class DetailAdapter(private val viewModel: DetailViewModel) :
    RecyclerView.Adapter<DetailViewHolder>() {

    private val forecast: MutableList<DailyForecast> = mutableListOf()

    override fun getItemViewType(position: Int) = R.layout.item_detail

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DetailViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) =
        holder.bind(forecast[position], viewModel)

    override fun getItemCount(): Int = forecast.size

    fun updateData(items: List<DailyForecast>) {
        val diffCallback = DetailItemDiffCallback(forecast, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        forecast.clear()
        forecast.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }
}