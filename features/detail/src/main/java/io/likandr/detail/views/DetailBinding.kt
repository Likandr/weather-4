package io.likandr.detail.views

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.likandr.common.extension.textDoubleRoundToInt
import io.likandr.model.entity.DailyForecastEntity
import io.likandr.repository.utils.Resource
import java.text.SimpleDateFormat
import java.util.*

object DetailBinding {

    @BindingAdapter("app:showWhenLoading")
    @JvmStatic
    fun <T> showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
        Log.d(DetailBinding::class.java.simpleName, "Resource: $resource")
        if (resource != null) view.isRefreshing = resource.status == Resource.Status.LOADING
    }

    @BindingAdapter("app:items")
    @JvmStatic
    fun setItems(recyclerView: RecyclerView, resource: Resource<DailyForecastEntity>?) {
        with(recyclerView.adapter as DetailAdapter?) {
            resource?.data?.list?.let { this?.updateData(it) }
        }
    }

    @BindingAdapter("app:roundValueAndSetText")
    @JvmStatic
    fun roundToInt(view: TextView, double: Double) {
        view.textDoubleRoundToInt(double)
    }

    @BindingAdapter("app:showWhenEmptyList")
    @JvmStatic
    fun showMessageErrorWhenEmptyList(view: View, resource: Resource<DailyForecastEntity>?) {
        if (resource != null) {
            view.visibility = if (resource.status == Resource.Status.ERROR
                && resource.data != null
                && resource.data!!.list.isEmpty()
            ) View.VISIBLE else View.GONE
        }
    }

    @BindingAdapter("app:textDate")
    @JvmStatic
    fun showDate(view: TextView, time: Long) {
        val formatter = SimpleDateFormat("dd/MM", Locale.getDefault())
        view.text = formatter.format(Date(time * 1000L))
    }
}