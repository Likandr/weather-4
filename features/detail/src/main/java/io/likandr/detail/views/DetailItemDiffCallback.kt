package io.likandr.detail.views

import androidx.recyclerview.widget.DiffUtil
import io.likandr.model.DailyForecast

class DetailItemDiffCallback(
    private val oldList: List<DailyForecast>,
    private val newList: List<DailyForecast>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].weather == newList[newItemPosition].weather
    }
}