package io.likandr.detail.views

import androidx.recyclerview.widget.RecyclerView
import io.likandr.detail.DetailViewModel
import io.likandr.detail.databinding.ItemDetailBinding
import io.likandr.model.DailyForecast

class DetailViewHolder(private val binding: ItemDetailBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: DailyForecast, viewModel: DetailViewModel) {
        binding.data = data
        binding.viewmodel = viewModel
        binding.executePendingBindings()
    }
}