package io.likandr.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.likandr.common.base.BaseFragment
import io.likandr.common.base.BaseViewModel
import io.likandr.home.databinding.FragmentHomeBinding
import io.likandr.home.views.HomeAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {

    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        setUpToolbar()
        return binding.root
    }

    private fun setUpToolbar() {
        setHasOptionsMenu(true)
        val activity = activity as AppCompatActivity
        val toolbar: Toolbar = binding.fragmentHomeToolbar
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureRecyclerView()
    }

    override fun getViewModel(): BaseViewModel = viewModel

    private fun configureRecyclerView() {
        val recyclerView = binding.fragmentHomeRv
        recyclerView.adapter = HomeAdapter(viewModel)
    }
}