package io.likandr.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.likandr.common.WeatherPreferences
import io.likandr.common.base.BaseViewModel
import io.likandr.common.utils.Event
import io.likandr.home.domain.GetCitiesUseCase
import io.likandr.model.entity.CityFavEntity
import io.likandr.repository.AppDispatchers
import io.likandr.repository.utils.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val getCitiesUseCase: GetCitiesUseCase,
    private val prefs: WeatherPreferences,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    private val _citiesFav = MediatorLiveData<Resource<List<CityFavEntity>>>()
    val citiesFav: LiveData<Resource<List<CityFavEntity>>> get() = _citiesFav
    private var citiesFavSource: LiveData<Resource<List<CityFavEntity>>> = MutableLiveData()

    init {
        getCities(false)
    }

    //region PUBLIC ACTIONS
    fun clicksOnItem(cityEntity: CityFavEntity) = navigate(
        HomeFragmentDirections.actionHomeFragmentToDetailFragment(
            cityEntity.id,
            cityEntity.name
        )
    )
    fun searchCity() = navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment())
    fun refreshesItems() = getCities(true)
    //endregion

    private fun getCities(forceRefresh: Boolean) = viewModelScope.launch(dispatchers.main) {
        _citiesFav.removeSource(citiesFavSource)
        withContext(dispatchers.io) {
            citiesFavSource = getCitiesUseCase(
                forceRefresh,
                prefs.favoriteCities?.toList()!!,
                ""
            )
        }
        _citiesFav.addSource(citiesFavSource) {
            _citiesFav.value = it
            if (it.status == Resource.Status.ERROR) _snackbarError.value =
                Event(R.string.an_error_happened)
        }
    }
}