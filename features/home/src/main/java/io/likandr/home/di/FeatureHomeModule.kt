package io.likandr.home.di

import io.likandr.home.HomeViewModel
import io.likandr.home.domain.GetCitiesUseCase
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureHomeModule = module {
    factory { GetCitiesUseCase(get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
}