package io.likandr.home.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import io.likandr.model.entity.CityFavEntity
import io.likandr.repository.CityRepository
import io.likandr.repository.CityRepositoryImpl
import io.likandr.repository.utils.Resource

/**
 * Use case that gets a [Resource][List][CityFavEntity] from [CityRepository]
 */
class GetCitiesUseCase(private val repository: CityRepositoryImpl) {

    suspend operator fun invoke(
        forceRefresh: Boolean = false,
        ids_of_city: List<String>,
        lng: String
    ): LiveData<Resource<List<CityFavEntity>>> {
        return repository.getCityWithCache(forceRefresh, ids_of_city, lng)
    }
}