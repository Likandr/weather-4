package io.likandr.home.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.likandr.home.HomeViewModel
import io.likandr.home.R
import io.likandr.model.entity.CityFavEntity

class HomeAdapter(private val viewModel: HomeViewModel) :
    RecyclerView.Adapter<HomeViewHolder>() {

    private val cities: MutableList<CityFavEntity> = mutableListOf()

    init {
        setHasStableIds(true)
    }

    override fun getItemViewType(position: Int): Int = R.layout.item_home

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HomeViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) =
        holder.bind(cities[position], viewModel)

    override fun getItemCount(): Int = cities.size
    override fun getItemId(position: Int): Long = position.toLong()

    fun updateData(items: MutableList<CityFavEntity>) {
        val diffCallback = HomeItemDiffCallback(cities, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        cities.clear()
        cities.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }
}
