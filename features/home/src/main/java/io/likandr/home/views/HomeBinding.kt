package io.likandr.home.views

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.likandr.common.extension.textDoubleRoundToInt
import io.likandr.model.entity.CityFavEntity
import io.likandr.repository.utils.Resource

object HomeBinding {

    @BindingAdapter("app:showWhenLoading")
    @JvmStatic
    fun <T> showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
        Log.d(HomeBinding::class.java.simpleName, "Resource: $resource")
        if (resource != null) view.isRefreshing = resource.status == Resource.Status.LOADING
    }

    @BindingAdapter("app:items")
    @JvmStatic
    fun setItems(recyclerView: RecyclerView, resource: Resource<MutableList<CityFavEntity>>?) {
        with(recyclerView.adapter as HomeAdapter?) {
            resource?.data?.let { this?.updateData(it) }
        }
    }

    @BindingAdapter("app:roundValueAndSetText")
    @JvmStatic
    fun roundToInt(view: TextView, double: Double) {
        view.textDoubleRoundToInt(double)
    }

    @BindingAdapter("app:showWhenEmptyList")
    @JvmStatic
    fun showMessageErrorWhenEmptyList(view: View, resource: Resource<List<CityFavEntity>>?) {
        if (resource != null) {
            view.visibility = if (resource.status == Resource.Status.ERROR
                && resource.data != null
                && resource.data!!.isEmpty()
            ) View.VISIBLE else View.GONE
        }
    }
}