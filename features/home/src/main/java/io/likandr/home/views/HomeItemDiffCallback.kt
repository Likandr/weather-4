package io.likandr.home.views

import androidx.recyclerview.widget.DiffUtil
import io.likandr.model.entity.CityFavEntity

class HomeItemDiffCallback(
    private val oldList: List<CityFavEntity>,
    private val newList: List<CityFavEntity>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].name == newList[newItemPosition].name
    }
}