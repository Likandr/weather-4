package io.likandr.home.views

import androidx.recyclerview.widget.RecyclerView
import io.likandr.home.HomeViewModel
import io.likandr.home.databinding.ItemHomeBinding
import io.likandr.model.entity.CityFavEntity

class HomeViewHolder(private val binding: ItemHomeBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: CityFavEntity, viewModel: HomeViewModel) {
        binding.data = data
        binding.viewmodel = viewModel
        binding.executePendingBindings()
    }
}