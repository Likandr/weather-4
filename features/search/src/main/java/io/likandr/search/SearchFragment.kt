package io.likandr.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import io.likandr.common.base.BaseFragment
import io.likandr.common.base.BaseViewModel
import io.likandr.common.extension.hideKeyboard
import io.likandr.common.utils.DebouncingQueryTextListener
import io.likandr.search.databinding.FragmentSearchBinding
import io.likandr.search.views.SearchAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment() {

    private val viewModel: SearchViewModel by viewModel()
    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        this.setUpToolbar()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureRecyclerView()
    }

    override fun getViewModel(): BaseViewModel = viewModel

    private fun setUpToolbar() {
        NavigationUI.setupWithNavController(binding.fragmentSearchToolbar, findNavController())

        val activity = activity as AppCompatActivity
        val toolbar: Toolbar = binding.fragmentSearchToolbar
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)

        bindSearch()
    }

    private fun bindSearch() {
        binding.search.isActivated = true
        binding.search.onActionViewExpanded()
        binding.search.isIconified = false
        binding.search.setOnQueryTextListener(activity?.lifecycle?.let {
            DebouncingQueryTextListener(lifecycle) { newText ->
                newText?.let { txt ->
                    if (txt.isEmpty()) {
                        viewModel.resetSearch()
                    } else {
                        viewModel.searchCity(txt)
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.hideKeyboard()
                findNavController().navigate(R.id.homeFragment); true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun configureRecyclerView() {
        binding.fragmentHomeRv.adapter = SearchAdapter(viewModel)
    }
}