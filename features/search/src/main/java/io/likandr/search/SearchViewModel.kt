package io.likandr.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.likandr.common.WeatherPreferences
import io.likandr.common.base.BaseViewModel
import io.likandr.common.utils.Event
import io.likandr.model.entity.CityEntity
import io.likandr.repository.AppDispatchers
import io.likandr.repository.utils.Resource
import io.likandr.search.domain.SearchCityUseCase
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchViewModel(
    private val searchCityUseCase: SearchCityUseCase,
    private val prefs: WeatherPreferences,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    private val _cities = MediatorLiveData<Resource<List<CityEntity>>>()
    val cities: LiveData<Resource<List<CityEntity>>> get() = _cities
    private var citiesSource: LiveData<Resource<List<CityEntity>>> = MutableLiveData()

    //region PUBLIC ACTIONS
    fun searchCity(cityName: String) = getCities(cityName)

    fun resetSearch() = clearResult()

    fun clicksOnItem(city: CityEntity) = saveCity(city)
    //endregion

    private fun getCities(cityName: String) = viewModelScope.launch(dispatchers.main) {
        _cities.removeSource(citiesSource)
        withContext(dispatchers.io) {
            citiesSource = searchCityUseCase(cityName = cityName)
        }
        _cities.addSource(citiesSource) {
            _cities.value = it
            if (it.status == Resource.Status.ERROR) _snackbarError.value =
                Event(R.string.an_error_happened)
        }
    }

    private fun clearResult() {}

    private fun saveCity(city: CityEntity) {
        val set = prefs.favoriteCities?.toMutableList()
        if (set != null) {
            set.add(city.id)
            prefs.favoriteCities = set.toSet()
        }
    }
}