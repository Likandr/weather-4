package io.likandr.search.di

import io.likandr.search.SearchViewModel
import io.likandr.search.domain.SearchCityUseCase
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureSearchModule = module {
    factory { SearchCityUseCase(get()) }
    viewModel { SearchViewModel(get(), get(), get()) }
}