package io.likandr.search.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import io.likandr.model.entity.CityEntity
import io.likandr.repository.CityRepositoryImpl
import io.likandr.repository.utils.Resource

class SearchCityUseCase(private val repository: CityRepositoryImpl) {

    suspend operator fun invoke(cityName: String): LiveData<Resource<List<CityEntity>>> {
        return repository.searchCityNoCache(cityName)
    }
}