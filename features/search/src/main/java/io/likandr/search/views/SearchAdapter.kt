package io.likandr.search.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.likandr.model.entity.CityEntity
import io.likandr.search.R
import io.likandr.search.SearchViewModel

class SearchAdapter(private val viewModel: SearchViewModel) :
    RecyclerView.Adapter<SearchViewHolder>() {

    private val cities: MutableList<CityEntity> = mutableListOf()

    override fun getItemViewType(position: Int) = R.layout.item_search

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SearchViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) =
        holder.bind(cities[position], viewModel)

    override fun getItemCount(): Int = cities.size

    fun updateData(items: List<CityEntity>) {
        val diffCallback = SearchItemDiffCallback(cities, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        cities.clear()
        cities.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }
}
