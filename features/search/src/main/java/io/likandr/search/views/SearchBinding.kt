package io.likandr.search.views

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import io.likandr.common.extension.textDoubleRoundToInt
import io.likandr.model.entity.CityEntity
import io.likandr.repository.utils.Resource

object SearchBinding {

    @BindingAdapter("app:items")
    @JvmStatic
    fun setItems(recyclerView: RecyclerView, resource: Resource<List<CityEntity>>?) {
        with(recyclerView.adapter as SearchAdapter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("app:roundValueAndSetText")
    @JvmStatic
    fun roundToInt(view: TextView, double: Double) {
        view.textDoubleRoundToInt(double)
    }

    @BindingAdapter("app:showWhenEmptyList")
    @JvmStatic
    fun showMessageErrorWhenEmptyList(view: View, resource: Resource<List<CityEntity>>?) {
        if (resource != null) {
            view.visibility = if (resource.status == Resource.Status.ERROR
                && resource.data != null
                && resource.data!!.isEmpty()
            ) View.VISIBLE else View.GONE
        }
    }
}