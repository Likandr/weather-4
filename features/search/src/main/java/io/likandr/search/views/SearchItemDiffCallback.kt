package io.likandr.search.views

import androidx.recyclerview.widget.DiffUtil
import io.likandr.model.entity.CityEntity

class SearchItemDiffCallback(
    private val oldList: List<CityEntity>,
    private val newList: List<CityEntity>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].name == newList[newItemPosition].name
    }
}