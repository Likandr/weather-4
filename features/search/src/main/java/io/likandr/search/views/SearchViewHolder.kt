package io.likandr.search.views

import androidx.recyclerview.widget.RecyclerView
import io.likandr.model.entity.CityEntity
import io.likandr.search.SearchViewModel
import io.likandr.search.databinding.ItemSearchBinding

class SearchViewHolder(private val binding: ItemSearchBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(city: CityEntity, viewModel: SearchViewModel) {
        binding.data = city
        binding.viewmodel = viewModel
        binding.executePendingBindings()
    }
}